    // var ctx = "http://192.168.199.202:3519";
	var ctx = '';
	var base_uri = "/rest/v1/audit/pad/";


	jQuery.extend({
		sendAjaxReq: function(requestType, restUrl, content, tCallBack, fCallBack) {
			$.ajax({
				headers: {
					"Basic-User": "restful",
					"Authorization": "Basic f0d9b321ff5a42c0039ef0bf1dd4c9fa",
					"Realm": "Local"
				},
				type: requestType,
				url: restUrl,
				data: content == null ? '' : JSON.stringify(content), //data : JSON.stringify(content),
				contentType: 'application/json;charset=UTF-8',
				success: function(data, textStatus) {
					if (tCallBack) {
						tCallBack(data, textStatus);
					}
				},
				error: function(xmlhttp, textStatus, errorThrown) {
					if (fCallBack) {
						fCallBack(xmlhttp, textStatus, errorThrown);
					}
				}
			});
		},



	});
