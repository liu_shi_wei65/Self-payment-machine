var qrcodeId = 0;
var toutFlag = 0;
var thisCube = 0;
var nt_flag = false;
var t = 30;//设定跳转的时间
var qx = 90;//设定取消购水跳转的时间

/*调用读卡器接口读取卡号返回*/
function read_id() {
    xajqReadId()
    if (nt_flag) {
        read_ntCard()
    }
};

function xajqReadId() {
    $.ajax({
        url: "http://127.0.0.1:21120/client/r_id",
        type: 'post',
        dataType: 'text',
        data: {},
        async: false,
        contentType: "application/x-www-form-urlencoded",
        traditional: true, //traditional 为true阻止深度序列化
        success: function (data) {
            data = JSON.parse(data)
            if (data.result == 1) {
                nt_flag = false;
                iccode = data.kahao
                console.info(iccode)
                selectMsgOne(iccode);
            } else {
                nt_flag = true;
            }
        },
        error: function () {
            //alert("读卡失败，请联系工作人员");
        }
    })
}

function read_ntCard() {
    var dd = {"meterType": "NT_IC_WATER_METER"};
    $.ajax({
        url: "http://127.0.0.1:11280/api/readCard",
        type: 'post',
        dataType: 'json',
        data: JSON.stringify(dd),
        timeout: 10000,
        async: false,
        contentType: "application/json",
        success: function (data) {
            console.info(data)
            if (data.code != 500) {
                nt_flag = true;
                iccode = data.data.MeterID;
                console.info(iccode)
                if (data.data.Status==0 ){
                    alert("水卡上次未刷表，请刷表后再购水！")
                }else {
                    selectMsgOne(iccode);
                }
            } else {
                nt_flag = false;
            }
        }
    })
}

/*插卡查询水价*/
function selectMsgOne(iccode) {
    $.ajax({
        url: "http://bchysw.natapp1.cc/payMachine/clientMsgOne",
        type: 'post',
        dataType: 'text',
        data: {"icCode": iccode},
        async: false,
        contentType: "application/x-www-form-urlencoded",
        traditional: true, //traditional 为true阻止深度序列化
        success: function (data) {
            data = JSON.parse(data)
            console.info(data)
            if (data.code == 0) {
                //设置全局水单价
                unitPrice = data.data.unitPrice;

                if (nt_flag){
                    $("#user_name").html(data.data.name);
                    $("#village_community").html(data.data.address)
                    $("#ic_code").html(data.data.icCode)

                    $("#user_name_jhm").html(data.data.name);
                    $("#village_community_jhm").html(data.data.address)
                    $("#ic_code_jhm").html(data.data.icCode)

                    $("#user_name_pay").html(data.data.name);
                    $("#village_community_pay").html(data.data.address)
                    $("#ic_code_pay").html(data.data.icCode)

                    $(".homePage").hide();
                    $(".buyPage").show();
                } else {
                    //判断是否刷表0：已刷表 1：未刷表
                    var sbFlag = readXAJQICCard(iccode, data.data.useCube)
                    console.info("sbFlag:" + sbFlag)
                    //todo 下面一行临时测试用，要删除的！
                    //sbFlag = 0
                    if (sbFlag == 1) {
                        alert("水卡上次未刷表，请刷表后再购水！")
                    } else {
                        $("#user_name").html(data.data.name);
                        $("#village_community").html(data.data.address)
                        $("#ic_code").html(data.data.icCode)

                        $("#user_name_jhm").html(data.data.name);
                        $("#village_community_jhm").html(data.data.address)
                        $("#ic_code_jhm").html(data.data.icCode)

                        $("#user_name_pay").html(data.data.name);
                        $("#village_community_pay").html(data.data.address)
                        $("#ic_code_pay").html(data.data.icCode)

                        $(".homePage").hide();
                        $(".buyPage").show();
                    }
                }

            } else if (data.code == 500) {
                alert(data.msg)
            }
        },
        error: function () {
        }
    })
};

function exit_page() {
    unitPrice = 0;
    iccode = 0;
    cube = 0;
    payPrice = 0;
    qrcodeId = 0;
    toutFlag = 0;
    thisCube = 0;
    nt_flag = false;
    t = 30;//设定跳转的时间
    qx = 30//设定取消购水跳转的时间
    location.reload();
}

/*聚合码生成订单*/
function buyWaterByJHM(iccode, price, cube) {
    if (price != 0) {
        var uuid = getUUID();
        var terminalId = $("#terminalId").html();
        $.ajax({
            url: "http://bchysw.natapp1.cc/payMachine/payQrcode",
            type: 'post',
            dataType: 'text',
            data: {"icCode": iccode, "price": price, "cube": cube, "uuid": uuid, "terminalId": terminalId},
            async: false,
            contentType: "application/x-www-form-urlencoded",
            traditional: true, //traditional 为true阻止深度序列化
            success: function (data) {
                data = JSON.parse(data)
                console.info(data)
                if (data.code == 0) {
                    thisCube = cube;
                    console.info(data.msg)
                    $(".homePage").hide();
                    $(".buyPage").hide();
                    $(".jhmPage").show();
                    $("#code").qrcode(data.msg);
                    playPay();//播放音频
                    $("#price_jhm").html(price)
                    $("#buy_price_pay").html(price)
                    qrcodeStatus(uuid);
                    setInterval("quxiaogoushui()", 1000); //启动1秒定时
                } else if (data.code == 500) {
                    alert(data.msg)
                }
            },
            error: function () {
            }
        })
    } else {
        alert("请选择购水量！")
    }

};

/*更新支付状态*/
function qrcodeStatus(uuid) {
    qrcodeId = uuid;
    $.ajax({
        url: "http://bchysw.natapp1.cc/payMachine/qrcodeStatus",
        type: 'post',
        dataType: 'text',
        data: {"uuid": uuid},
        async: false,
        contentType: "application/x-www-form-urlencoded",
        traditional: true, //traditional 为true阻止深度序列化
        success: function (data) {
            data = JSON.parse(data)
            console.info(data)
            if (data.msg == "success") {
                toutFlag = 1;
                console.info("充值：" + iccode + "-thisCube:" + thisCube + "-use_cube:" + data.data.use_cube)
                console.info("当前nt_flag：" + nt_flag)
                if (nt_flag) {
                    console.info("进入chongzhiNTICCard")
                    chongzhiNTICCard(iccode, thisCube, data.data.use_cube)
                } else {
                    console.info("进入chongzhiXAJQICCard")
                    chongzhiXAJQICCard(iccode, thisCube, data.data.use_cube)
                }
            }
        },
        error: function () {
        },
        complete: function (XHR, TS) {
            console.info("调用！")
            //回收资源
            XHR = null;
            //3秒后再调用
            if (toutFlag != 1) {
                console.info("qrcodeId:" + qrcodeId)
                setTimeout("qrcodeStatus(qrcodeId)", 3000);
            }
        }
    })
}

/*读卡*/
function readXAJQICCard(code, useCube) {
    var flag = 0;
    console.info("调用")
    var data_json = {
        "kahao": code,
        "kabanben": "1",
        "meters": [{
            "fenleihao": "1",
            "biaohao": "",
            "biaoxing": "203",
            "bj1": "10.0",
            "bj2": "5.0",
            "cishu": "1",
            "benci": "0",
            "leiji": useCube,
            "shengyu": "0",
            "tunji": "9000.00",
            "zhuangtai": "",
            "action": "2",
            "moshi": "0",
            "canshubanben": "1",
            "stepdatetime": "2020-01-01 00:00:00",
            "m1": "0",
            "m2": "0",
            "m3": "0",
            "m4": "0",
            "p1": "0.000",
            "p2": "0.000",
            "p3": "0.000",
            "p4": "0.000",
            "usenum": "0.00"
        }]
    }
    $.ajax({
        type: "POST",
        url: "http://127.0.0.1:21120/client/r_data",
        dataType: "json",
        async: false,
        timeout: 5000, //超时时间设置，单位毫秒
        data: JSON.stringify(data_json),
        success: function (data) {
            console.info("data:" + JSON.stringify(data))
            if (data.result == 1) {
                console.info("zhuangtai:" + data.meters[0].zhuangtai)
                flag = data.meters[0].zhuangtai;
            } else if (data.result == -1) {
                flag = readXAJQSPCard(code, useCube)
            }
        }
    })
    console.info(data_json)
    return flag
}

/*读卡*/
function readXAJQSPCard(code, useCube) {
    var flag = 0;
    console.info("调用")
    var data_json = {
        "kahao": code,
        "kabanben": "1",
        "meters": [{
            "fenleihao": "1",
            "biaohao": "",
            "biaoxing": "219",
            "bj1": "10.0",
            "bj2": "5.0",
            "cishu": "1",
            "benci": "0",
            "leiji": useCube,
            "shengyu": "0",
            "tunji": "9000.00",
            "zhuangtai": "",
            "action": "2",
            "moshi": "2",
            "canshubanben": "1",
            "stepdatetime": "2020-01-01 00:00:00",
            "m1": "5000",
            "m2": "5000",
            "m3": "5000",
            "m4": "5000",
            "p1": unitPrice,
            "p2": unitPrice,
            "p3": unitPrice,
            "p4": unitPrice,
            "usenum": "0.00"
        }]
    }
    $.ajax({
        type: "POST",
        url: "http://127.0.0.1:21120/client/r_data",
        dataType: "json",
        async: false,
        timeout: 5000, //超时时间设置，单位毫秒
        data: JSON.stringify(data_json),
        success: function (data) {
            console.info("data:" + JSON.stringify(data))
            if (data.result == 1) {
                console.info("zhuangtai:" + data.meters[0].zhuangtai)
                flag = data.meters[0].zhuangtai;
            }
        }
    })
    console.info(data_json)
    return flag
}

/*写卡*/
function chongzhiXAJQICCard(code, thiscube, allcube) {
    console.info("调用")
    var data_json = {
        "kahao": code,
        "kabanben": "1",
        "meters": [{
            "fenleihao": "1",
            "biaohao": "",
            "biaoxing": "203",
            "bj1": "10.0",
            "bj2": "5.0",
            "cishu": "1",
            "benci": thiscube,
            "leiji": allcube,
            "shengyu": "0",
            "tunji": "9000.00",
            "zhuangtai": "0",
            "action": "2",
            "moshi": "0",
            "canshubanben": "1",
            "stepdatetime": "2020-01-01 00:00:00",
            "m1": "0",
            "m2": "0",
            "m3": "0",
            "m4": "0",
            "p1": "0.000",
            "p2": "0.000",
            "p3": "0.000",
            "p4": "0.000",
            "usenum": "0.00"
        }]
    }
    $.ajax({
        type: "POST",
        url: "http://127.0.0.1:21120/client/w_data",
        dataType: "json",
        timeout: 5000, //超时时间设置，单位毫秒
        data: JSON.stringify(data_json),
        success: function (data) {
            console.info("data:" + JSON.stringify(data))
            if (data.result == 1) {
                $("#buy_cube_pay").html(thiscube)
                $(".homePage").hide();
                $(".buyPage").hide();
                $(".jhmPage").hide();
                $(".paySuccessPage").show();
            } else if (data.result == -1) {
                chongzhiSPCard(code, thiscube, allcube)
            }
        }
    })
    console.info(data_json)
}

function chongzhiSPCard(code, thiscube, allcube) {
    console.info("调用")
    var data_json = {
        "kahao": code,
        "kabanben": "1",
        "meters": [{
            "fenleihao": "1",
            "biaohao": "",
            "biaoxing": "219",
            "bj1": "20.0",
            "bj2": "10.0",
            "cishu": "1",
            "benci": thiscube * unitPrice,
            "leiji": allcube,
            "shengyu": "0",
            "tunji": "9000.00",
            "zhuangtai": "0",
            "action": "2",
            "moshi": "2",
            "canshubanben": "1",
            "stepdatetime": "2020-01-01 00:00:00",
            "m1": "5000",
            "m2": "5000",
            "m3": "5000",
            "m4": "5000",
            "p1": unitPrice,
            "p2": unitPrice,
            "p3": unitPrice,
            "p4": unitPrice,
            "usenum": "0.00"
        }]
    }

    //var data_json = {"kahao":code,"kabanben":"1","meters":[{"fenleihao":"1","biaohao":"","biaoxing":"219","bj1":"20.0","bj2":"10.0","cishu":"1","benci":thiscube*unitPrice,"leiji":allcube*unitPrice,"shengyu":"0","tunji":"9000.00","zhuangtai":"0","action":"2","moshi":"2","canshubanben":"1","stepdatetime":"2020-01-01 00:00:00","m1":"99999","p1":unitPrice,"usenum":"0.00"}]}
    console.info('data_json:' + data_json)
    $.ajax({
        type: "POST",
        url: "http://127.0.0.1:21120/client/w_data",
        dataType: "json",
        timeout: 5000, //超时时间设置，单位毫秒
        data: JSON.stringify(data_json),
        success: function (data) {
            console.info("data:" + JSON.stringify(data))
            if (data.result == 1) {
                $("#buy_cube_pay").html(thiscube)
                $(".homePage").hide();
                $(".buyPage").hide();
                $(".jhmPage").hide();
                $(".paySuccessPage").show();
            } else if (data.result == -1) {
                alert("写卡失败，请联系工作人员柜台充值...")
            }
        }
    })
    console.info(data_json)
}

function chongzhiNTICCard(code, thiscube, allcube) {
    var clientId = code;//用户号
    var ntCount = null;
    var url = "http://bchysw.natapp1.cc/ClientMsg/ClientMsg/ntNextIcCount";
    $.ajax({
        url: url,
        type: 'post',
        dataType: 'text',
        data: {"clientId": clientId},
        async: false,
        contentType: "application/x-www-form-urlencoded",
        traditional: true, //traditional 为true阻止深度序列化
        success: function (data) {
            data = JSON.parse(data);
            ntCount = data.msg;
        }
    })


    var dd = {
        "meterType": "NT_IC_WATER_METER",
        "param": {
            "areaCode": 1,
            "userCode": code,
            "childNo": 0,
            "buyValue": thiscube * 10,
            "limitValue": 1000,
            "count": ntCount - 1
        }
    }
    $.ajax({
        url: "http://127.0.0.1:11280/api/buy",
        type: 'post',
        dataType: 'json',
        data: JSON.stringify(dd),
        async: false,
        contentType: "application/json",
        success: function (data) {
            console.info(data)
            if (data.code == 500) {
                alert("写卡失败，请联系工作人员柜台充值...")
            } else if (data.code == 200) {
                $("#buy_cube_pay").html(thiscube)
                $(".homePage").hide();
                $(".buyPage").hide();
                $(".jhmPage").hide();
                $(".paySuccessPage").show();
                setInterval("refer()", 1000); //启动1秒定时
            }
        },
        error: function () {
        }
    })
}

function refer() {
    if (t == 0) {
        unitPrice = 0;
        iccode = 0;
        cube = 0;
        payPrice = 0;
        qrcodeId = 0;
        toutFlag = 0;
        thisCube = 0;
        nt_flag = false;
        t = 30;//设定跳转的时间
        qx = 90;//设定取消购水跳转的时间
        location.reload();
    }
    document.getElementById('exit_page').innerHTML = "(" + t + ") 退出"; // 显示倒计时
    t--; // 计数器递减

}

function quxiaogoushui() {
    if (qx == 0) {
        unitPrice = 0;
        iccode = 0;
        cube = 0;
        payPrice = 0;
        qrcodeId = 0;
        toutFlag = 0;
        thisCube = 0;
        nt_flag = false;
        t = 30;//设定跳转的时间
        qx = 90;//设定取消购水跳转的时间
        location.reload();
    }
    document.getElementById('cancelBy').innerHTML = "(" + qx + ") 取消购水"; // 显示倒计时
    qx--; // 计数器递减

}

function getUUID() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {

        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

